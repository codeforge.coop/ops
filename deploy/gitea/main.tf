terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
    random = {}
    tls    = {}
  }
}

data "digitalocean_image" "gitea" {
  name = "gitea"
}

resource "digitalocean_droplet" "gitea" {
  image             = data.digitalocean_image.gitea.id
  name              = "gitea"
  region            = var.region
  size              = var.size
  ipv6              = true
  ssh_keys          = var.authorized_keys
  graceful_shutdown = true
  tags              = var.tags
  vpc_uuid          = var.vpc_uuid
  # These should likely use a separate volume_attachment resource in the service
  # module, but DigitalOcean has a bug where you have to attach volumes to
  # FreeBSD instances at startup. If you use the API afterwards they sometimes
  # fail silently.
  volume_ids = var.volume_ids
}

locals {
  config = templatefile("${path.module}/app.ini", merge(var.config, {
    "internal_token" = base64encode(random_password.internal_token.result),
    "jwt_secret"     = base64encode(random_password.jwt_secret.result),
    "secret_key"     = random_password.secret_key.result,
    "metrics_token"  = random_password.metrics_token.result,
  }))
  caddyfile = templatefile("${path.module}/Caddyfile", var.config)
}

resource "null_resource" "caddy_install" {
  triggers = {
    droplet_ids = digitalocean_droplet.gitea.urn
    config      = sha256(local.caddyfile)
  }

  connection {
    type = "ssh"
    user = "freebsd"
    host = digitalocean_droplet.gitea.ipv4_address

    bastion_host = var.bastion_host
  }

  provisioner "file" {
    destination = "/tmp/Caddyfile"
    content     = local.caddyfile
  }

  provisioner "remote-exec" {
    inline = [
      "sudo sysrc caddy_enable=YES",
      "sudo mv /tmp/Caddyfile /usr/local/etc/caddy/Caddyfile",
      "sudo service caddy restart",
    ]
  }
}

resource "null_resource" "gitea_install" {
  triggers = {
    droplet_ids = digitalocean_droplet.gitea.urn
    config      = sha256(local.config)
  }

  connection {
    type = "ssh"
    user = "freebsd"
    host = digitalocean_droplet.gitea.ipv4_address

    bastion_host = var.bastion_host
  }

  provisioner "file" {
    destination = "/tmp/app.ini"
    content     = local.config
  }

  provisioner "remote-exec" {
    inline = [
      "sudo sysrc gitea_enable=YES",
      "sudo mv /tmp/app.ini /usr/local/etc/gitea/conf/app.ini",
      "sudo chown git:git /var/run/gitea.pid",
      "sudo pw unlock root",
      "sudo -u git gitea --config /usr/local/etc/gitea/conf/app.ini admin regenerate keys",
      "sudo service gitea restart",
    ]
  }
}
