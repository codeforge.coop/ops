variable "region" {
  description = "The digital ocean region to deploy to."
  type        = string
}

variable "size" {
  description = "The unique slug that indentifies the type of Droplet. You can find a list of available slugs at https://docs.digitalocean.com/reference/api/api-reference/#tag/Sizes."
  type        = string
}

variable "authorized_keys" {
  description = "A list of SSH key IDs or fingerprints to enable. Requires re-creation if changed."
  type        = list(string)
  default     = []
}

variable "tags" {
  description = "Tags to apply to the droplet."
  type        = list(string)
  default     = []
}

variable "config" {
  description = "Values to be placed in the Gitea config file."
  type = object({
    display_name = string
    name         = string
    domain       = string
    smtp_pass    = string
    db_host      = string
    db_name      = string
    db_port      = number
    db_user      = string
    db_pass      = string
  })
}

variable "vpc_uuid" {
  description = "UUID of the VPC where the droplet will be located."
  type        = string
}

variable "bastion_host" {
  description = "The hostname of a bastion server to proxy SSH through."
  type        = string
  default     = ""
}

variable "volume_ids" {
  description = "A list of the IDs of each block storage volume to be attached to the Droplet."
  type        = list(string)
  default     = []
}
