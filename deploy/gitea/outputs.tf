#
# Outputs
#
output "id" {
  value = digitalocean_droplet.gitea.id
}

output "urn" {
  value = digitalocean_droplet.gitea.urn
}

output "ipv6_address" {
  value = digitalocean_droplet.gitea.ipv6_address
}

output "ipv4_address" {
  value = digitalocean_droplet.gitea.ipv4_address
}

output "ipv4_address_private" {
  value = digitalocean_droplet.gitea.ipv4_address_private
}
