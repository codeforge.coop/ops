locals {
  prom_config = templatefile("${path.module}/prometheus.yml", {
    "metrics_token" = random_password.metrics_token.result
  })
}

resource "null_resource" "prometheus_install" {
  triggers = {
    droplet_ids = digitalocean_droplet.gitea.urn
    config      = sha256(local.prom_config)
  }

  connection {
    type = "ssh"
    user = "freebsd"
    host = digitalocean_droplet.gitea.ipv4_address

    bastion_host = var.bastion_host
  }

  provisioner "file" {
    destination = "/tmp/prometheus.yml"
    content     = local.prom_config
  }

  provisioner "remote-exec" {
    inline = [
      "set -o errexit",
      "sudo sysrc prometheus_enable=YES",
      "sudo mv /tmp/prometheus.yml /usr/local/etc/prometheus.yml",
      "sudo service prometheus restart",
    ]
  }
}

resource "null_resource" "node_exporter_install" {
  triggers = {
    droplet_ids = digitalocean_droplet.gitea.urn
  }

  connection {
    type = "ssh"
    user = "freebsd"
    host = digitalocean_droplet.gitea.ipv4_address

    bastion_host = var.bastion_host
  }

  provisioner "remote-exec" {
    inline = [
      "set -o errexit",
      "sudo sysrc node_exporter_enable=YES",
      "sudo service node_exporter restart",
    ]
  }
}
