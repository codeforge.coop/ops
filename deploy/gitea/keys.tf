resource "random_password" "internal_token" {
  length  = 64
  special = true
}

resource "random_password" "secret_key" {
  length  = 32
  special = true
}

resource "random_password" "jwt_secret" {
  length  = 32
  special = true
}

resource "random_password" "metrics_token" {
  length  = 32
  special = true
}
