variable "smtp_pass" {
  description = "The password to use for SMTP access."
  type        = string
  sensitive   = true
}
