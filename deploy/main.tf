terraform {
  required_version = ">= 1.0.8"

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.16.0"
    }
    random = {
      version = "~> 3.1.0"
    }
    tls = {
      version = "~> 3.1.0"
    }
  }

  backend "pg" {}
}

provider "digitalocean" {
  # Use environment variable DIGITALOCEAN_TOKEN for config.
}

locals {
  region = "nyc3"
}

resource "digitalocean_project" "ops" {
  name        = "ops"
  description = "A project to represent operational resources that may be shared between deployments."
  environment = "Development"
}

module "prod" {
  env             = "prod"
  source          = "./service"
  region          = local.region
  size            = "s-1vcpu-1gb"
  authorized_keys = [for v in digitalocean_ssh_key.authorized_keys : v.id]
  smtp_pass       = var.smtp_pass
  #bastion_host    = digitalocean_droplet.bastion.ipv4_address
}
