resource "digitalocean_volume" "git" {
  region      = var.region
  name        = "repos"
  size        = 1
  description = "storage of git repos"
  lifecycle {
    prevent_destroy = true
  }
}

resource "null_resource" "git_format" {
  triggers = {
    volume_urn = digitalocean_volume.git.urn
  }

  connection {
    type = "ssh"
    user = "root"
    host = module.gitea.ipv4_address
  }

  provisioner "remote-exec" {
    inline = [
      "set -o errexit",
      "! fstyp /dev/da0",
      "unmount /var/db/gitea/",
      "newfs -L repos -J -U /dev/da0",
    ]
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "null_resource" "git_mount" {
  triggers = {
    droplet_ids = module.gitea.urn
    prev_step   = null_resource.git_format.id
  }

  connection {
    type = "ssh"
    user = "root"
    host = module.gitea.ipv4_address
  }

  provisioner "remote-exec" {
    inline = [
      "set -o errexit",
      "mkdir -p /var/db/gitea",
      "mount -o rw,noatime /dev/da0 /var/db/gitea/",
      "chown -R git:wheel /var/db/gitea/",
    ]
  }
}
