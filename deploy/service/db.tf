resource "digitalocean_database_cluster" "app" {
  name                 = "app"
  engine               = "pg"
  version              = "13"
  size                 = "db-s-1vcpu-1gb"
  region               = "nyc3"
  node_count           = 1
  private_network_uuid = digitalocean_vpc.internal.id
  tags                 = [digitalocean_tag.ssh.name]

  lifecycle {
    prevent_destroy = true
  }
}

resource "digitalocean_database_db" "gitea" {
  cluster_id = digitalocean_database_cluster.app.id
  name       = "gitea"

  lifecycle {
    prevent_destroy = true
  }
}

resource "digitalocean_tag" "db" {
  name = "db"

  lifecycle {
    prevent_destroy = true
  }
}

resource "digitalocean_database_firewall" "app" {
  cluster_id = digitalocean_database_cluster.app.id

  rule {
    type  = "tag"
    value = "db"
  }

  # Sam
  rule {
    type  = "ip_addr"
    value = "69.218.231.3"
  }

  lifecycle {
    prevent_destroy = true
  }
}
