resource "digitalocean_domain" "domain" {
  # TODO: update later. This is a temporary name I already own for testing.
  name = "mesh.coop"
}

resource "digitalocean_record" "mainv4" {
  domain = digitalocean_domain.domain.name
  type   = "A"
  name   = "@"
  value  = digitalocean_floating_ip.gitea.ip_address
}

resource "digitalocean_record" "mainv6" {
  domain = digitalocean_domain.domain.name
  type   = "AAAA"
  name   = "@"
  value  = module.gitea.ipv6_address
}

resource "digitalocean_record" "www" {
  domain = digitalocean_domain.domain.name
  type   = "CNAME"
  name   = "www"
  value  = "${digitalocean_record.mainv4.fqdn}."
}
