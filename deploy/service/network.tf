resource "digitalocean_vpc" "internal" {
  name        = "gitea-internal"
  description = "Our primary internal network."
  region      = var.region
  ip_range    = "10.10.10.0/24"
}

resource "digitalocean_tag" "web" {
  name = "web"
}

resource "digitalocean_tag" "ssh" {
  name = "ssh"
}

resource "digitalocean_firewall" "web" {
  name = "only-ssh-http"

  tags = [digitalocean_tag.web.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "80"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
  outbound_rule {
    protocol              = "tcp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
    port_range            = "all"
  }
  outbound_rule {
    protocol              = "udp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
    port_range            = "all"
  }
}

resource "digitalocean_firewall" "ssh" {
  name = "only-ssh"

  tags = [digitalocean_tag.ssh.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
  outbound_rule {
    protocol              = "tcp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
    port_range            = "all"
  }
  outbound_rule {
    protocol              = "udp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
    port_range            = "all"
  }
}

resource "digitalocean_floating_ip" "gitea" {
  region = var.region

  lifecycle {
    prevent_destroy = true
  }
}

resource "digitalocean_floating_ip_assignment" "gitea" {
  ip_address = digitalocean_floating_ip.gitea.ip_address
  droplet_id = module.gitea.id
}
