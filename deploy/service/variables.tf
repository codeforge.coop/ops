variable "region" {
  description = "The digital ocean region to deploy to."
  type        = string
}

variable "bastion_host" {
  description = "The hostname of a bastion server to proxy SSH through."
  type        = string
  default     = ""
}

variable "size" {
  description = "The unique slug that indentifies the type of Droplet. You can find a list of available slugs at https://docs.digitalocean.com/reference/api/api-reference/#tag/Sizes."
  type        = string
}

variable "authorized_keys" {
  description = "A list of SSH key IDs or fingerprints to enable. Requires re-creation if changed."
  type        = list(string)
  default     = []
}

variable "env" {
  description = "The name of the environment."
  type        = string
}

variable "smtp_pass" {
  description = "The password to use for SMTP access."
  type        = string
  sensitive   = true
}
