terraform {
  required_version = ">= 1.0.8"

  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
    random = {}
    tls    = {}
  }
}

resource "digitalocean_project" "app" {
  name        = var.env
  description = "A project to represent ${var.env} resources."
  environment = var.env == "prod" ? "Production" : "Development"
}

resource "digitalocean_project_resources" "app" {
  project = digitalocean_project.app.id
  resources = [
    digitalocean_database_cluster.app.urn,
    digitalocean_domain.domain.urn,
    digitalocean_floating_ip.gitea.urn,
    module.gitea.urn,
  ]
}

resource "digitalocean_project_resources" "volume" {
  project = digitalocean_project.app.id
  resources = [
    # If we assign the volume and the floating_ip in the same API call, Digital
    # Ocean 500's for some reason.
    digitalocean_volume.git.urn,
  ]
}

data "digitalocean_image" "freebsd" {
  slug = "freebsd-12-x64-ufs"
}

#resource "digitalocean_droplet" "bastion" {
#  image             = data.digitalocean_image.freebsd.id
#  name              = "bastion"
#  region            = local.region
#  size              = "s-1vcpu-1gb"
#  ipv6              = true
#  ssh_keys          = [for v in digitalocean_ssh_key.authorized_keys : v.id]
#  graceful_shutdown = true
#  tags              = ["ssh"]
#  vpc_uuid          = digitalocean_vpc.internal.id
#}

module "gitea" {
  source          = "../gitea"
  region          = var.region
  size            = var.size
  authorized_keys = var.authorized_keys
  vpc_uuid        = digitalocean_vpc.internal.id
  tags            = [digitalocean_tag.web.name, digitalocean_tag.db.name]
  bastion_host    = var.bastion_host
  volume_ids      = [digitalocean_volume.git.id]

  config = {
    display_name = "Codeforge Co-op"
    name         = "codeforge"
    domain       = digitalocean_domain.domain.name
    smtp_pass    = var.smtp_pass
    db_name      = digitalocean_database_db.gitea.name
    db_host      = digitalocean_database_cluster.app.private_host
    db_port      = digitalocean_database_cluster.app.port
    db_user      = digitalocean_database_cluster.app.user
    db_pass      = digitalocean_database_cluster.app.password
  }
}
