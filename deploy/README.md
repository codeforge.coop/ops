# deploy

The `deploy/` tree contains descriptions of the infrastructure to be built using
[Terraform].

The first thing you'll need to do before deploying is grant yourself database
access.
This is only necessary temporarily (for more information see [issue #20]).
First modify the file `db.tf` to add a rule granting your IP address access (if
your IP changes you may have to modify this periodically).
Create a PR and either ask for it to be deployed in the PR or, if you have
Digital Ocean console access, log in and manually update the firewall to include
your IP and then deploy the changes yourself.

Next you'll need to initialize Terraform.
This will download the required plugins and try to connect to the database where
shared state information and deploy locks are kept.
If you just run `terraform init` without creating a config file it will ask you
for the database connection string.
If you don't want to be asked in the future, create a config file and populate
it with the database value (found in the web control panel):

    echo conn_str="postgres://doadmin:PASS@app-do-user-10377509-0.b.db.ondigitalocean.com:25060/terraform_backend" > config.pg.tfbackend
    terraform init -backend-config=config.pg.tfbackend

There are also other ways to provide Terraform with the connection string.
For more information see the Terraform docs on "[partial configuration]".

You will also need to set any variables described in the file `variables.tf`.
This can be done with a `.tfvars` file, or by setting environment variables
starting with `TF_VAR_`.
For more information see the docs for [input variables].

[Terraform]: https://www.terraform.io
[partial configuration]: https://www.terraform.io/language/settings/backends/configuration#partial-configuration
[issue #20]: https://codeberg.org/SamWhited/codeforge-ops/issues/20
[input variables]: https://www.terraform.io/language/values/variables
