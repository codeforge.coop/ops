resource "digitalocean_ssh_key" "authorized_keys" {
  for_each = {
    for v in split("\n", chomp(file("authorized_keys"))) : regex(".+ ([^ ]+)$", v).0 => v
  }
  name       = each.key
  public_key = each.value
}
