# Codeforge Ops

This is the operations repo for our unnamed codeforge.


## images

The `images/` tree contains [Packer] descriptions of the base machine images
form which the infrastructure in the `deploy/` tree will be created.

[Packer]: https://www.packer.io/


## deploy

The `deploy/` tree contains [Terraform] templates to spin up an instance of the
service on [DigitalOcean].

[Terraform]: https://www.terraform.io
[DigitalOcean]: https://www.digitalocean.com/
