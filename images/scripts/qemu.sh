#!/bin/sh
set -ex

pw mod user "root" -w none
pkg install -y gmake mercurial tmux vim-tiny
pw groupadd freebsd
adduser -G wheel -f - <<EOF
	freebsd:::::::/home/freebsd:/bin/tcsh:
EOF
echo "freebsd" | pw usermod freebsd -h0
mkdir -p /usr/local/etc/gitea/templates
pw groupmod operator -mfreebsd
sysrc gitea_enable=YES
sysrc caddy_enable=YES
