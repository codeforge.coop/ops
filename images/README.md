# images

The `images/` tree contains [Packer] descriptions of the base machine images
form which the infrastructure in the `deploy/` tree will be created.

To get started try:

    $ make run

This will launch a simple devvm with Gitea running at https://localhost:8443/.
An admin user named "gitea" with the same for a password has been created.

To build the devvm the following dependencies must be installed:

- [`packer`][Packer]
- [`qemu`]

The default username and password are both "freebsd" and SSH is forwarded on
port 2224 by default so you can access your devvm by running:

    $ ssh -p 2224 freebsd@localhost

If you'd like to show the display instead of using SSH you can set the
`QEMU_DISPLAY` option:

    make QEMU_DISPLAY=curses run
    make QEMU_DISPLAY=gtk,gl=on run

For more information see the [qemu user documentation][qemu-docs-display].

If you want a graphical frontend, [`gnome-boxes`] is recommended.

To log in to the service visit `http://localhost:3000`.
The default username and password are both "gitea".

[Packer]: https://www.packer.io/
[`gnome-boxes`]: https://wiki.gnome.org/Apps/Boxes
[`qemu`]: https://wiki.archlinux.org/index.php/QEMU
[qemu-docs-display]: https://www.qemu.org/docs/master/system/qemu-manpage.html#hxtool-3
