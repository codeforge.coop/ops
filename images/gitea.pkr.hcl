variables {
  freebsd_major = "13"
  freebsd_minor = "0"
  ssh_username  = "root"
  ssh_password  = "packer"
  iso_checksum  = "sha256:f78d4e5f53605592863852b39fa31a12f15893dc48cacecd875f2da72fa67ae5"
  outdir        = "output-qemu/"
  vm_name       = "packer-freebsd.qcow2"
}

locals {
  freebsd_version = "${var.freebsd_major}.${var.freebsd_minor}"
}

packer {
  required_plugins {
    digitalocean = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/digitalocean"
    }
  }
}

source "qemu" "freebsd" {
  iso_url          = "https://download.freebsd.org/ftp/releases/amd64/amd64/ISO-IMAGES/${local.freebsd_version}/FreeBSD-${local.freebsd_version}-RELEASE-amd64-disc1.iso"
  iso_checksum     = var.iso_checksum
  format           = "qcow2"
  accelerator      = "kvm"
  ssh_username     = var.ssh_username
  ssh_password     = var.ssh_password
  output_directory = var.outdir
  shutdown_command = "shutdown -p now"
  headless         = true
  http_directory   = "${path.root}/http"
  net_device       = "virtio-net"
  disk_interface   = "virtio"
  vm_name          = var.vm_name
  boot_command = [
    "<enter>",
    "<wait20s>",
    "<right>",
    "<enter>",
    "dhclient -p /tmp/dhclient.pid -l /tmp/dhclient.lease vtnet0<enter><wait5>",
    "fetch -o/tmp/installerconfig http://{{ .HTTPIP }}:{{ .HTTPPort }}/installerconfig<enter><wait>",
    "bsdinstall script /tmp/installerconfig; shutdown -r now<enter>",
  ]
}

source "digitalocean" "freebsd" {
  # Use DIGITALOCEAN_API_TOKEN to configure
  image = "freebsd-12-x64-ufs"
  region               = "nyc3"
  size                 = "s-1vcpu-1gb"
  ssh_username         = "root"
  snapshot_name        = "gitea"
  tags                 = ["ssh"]
  ipv6                 = true

  private_networking = true
  vpc_uuid           = "7db387a7-c4e1-43a6-ba9a-1c428fdbf26b"
}

build {
  sources = [
    "source.qemu.freebsd",
    "source.digitalocean.freebsd",
  ]
  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; env {{ .Vars }} {{ .Path }}"
    script          = "${path.root}/scripts/base.sh"
  }
  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; env {{ .Vars }} {{ .Path }}"
    script          = "${path.root}/scripts/qemu.sh"
    only            = ["qemu.freebsd"]
  }
  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; env {{ .Vars }} {{ .Path }}"
    script          = "${path.root}/scripts/cleanup.sh"
    except          = ["qemu.freebsd"]
  }
  provisioner "file" {
    source      = "${path.root}/app.ini"
    destination = "/usr/local/etc/gitea/conf/app.ini"
  }
  provisioner "file" {
    source      = "${path.root}/Caddyfile"
    destination = "/usr/local/etc/caddy/Caddyfile"
    only        = ["qemu.freebsd"]
  }
  provisioner "file" {
    source      = "${path.root}/public"
    destination = "/usr/local/etc/gitea"
  }
  provisioner "file" {
    source      = "${path.root}/templates"
    destination = "/usr/local/etc/gitea"
  }
  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; env {{ .Vars }} {{ .Path }}"
    inline = [
      "mkdir -p /usr/local/etc/gitea",
      "chown -R git:git /usr/local/etc/gitea",
      "mkdir -p /var/db/gitea",
      "chown -R git:git /var/db/gitea",
      "su git -c 'gitea -c /usr/local/etc/gitea/conf/app.ini migrate'",
      "su git -c 'gitea -c /usr/local/etc/gitea/conf/app.ini admin user create --email=freebsd@localhost --username=gitea --password=gitea --admin'",
    ]
    only = ["qemu.freebsd"]
  }
  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; env {{ .Vars }} {{ .Path }}"
    inline          = ["pw lock root"]
    except          = ["qemu.freebsd"]
  }
  post-processor "checksum" {
    checksum_types = ["sha256"]
    output         = "${var.outdir}/${var.vm_name}.checksum"
  }
}
